import React, { useState, useEffect } from "react";
import TodoItem from "./TodoItem";
import { useSelector } from "react-redux";
import { HOST } from "./confic";
import { useDispatch } from 'react-redux';
import { listTodo} from '../redux/actions';



function TodoList() {
  const [data, setdatas] = useState([]);
  const [creat, setCreat] = useState("");
  const dispatch = useDispatch();
  useEffect(() => {
    const fectchData = async () => {
      const result = await fetch("http://localhost:1337/list");
      const data = await result.json();
      dispatch(listTodo(data.todos));
    };
    fectchData();
  }, []);

  let todos = useSelector((state) => state);
  return (
    <div className="my-4">
      {todos.map((todo, index) => {
        return <TodoItem key={index} todo={todo} sTT={index} />;
      })}
    </div>
  );
}

export default TodoList;
