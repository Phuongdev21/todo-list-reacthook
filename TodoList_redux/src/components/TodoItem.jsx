import React, { useState } from 'react'
import { useDispatch } from 'react-redux';
import { deleteTodo, updateTodo } from '../redux/actions';

function TodoItem({ todo, sTT }) {
    const [editable, setEditable] = useState(false)
    const [name, setName] = useState(todo.name)
    let dispatch = useDispatch();

    return (
        <div>
            <div className="row mx-4 align-items-center">
                <div>{ sTT }</div>
                <div className="col">
                    {editable ?
                        <input type="text" className="form-control"
                            value={name}
                            onChange={(e) => {
                                setName(e.target.value);
                            }}

                        />
                        :
                        <h4>{todo.name}</h4>}
                </div>
                <button className="btn btn-primary m-2"
                    onClick={() => {
                        let object = {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify({
                                id: todo.id,
                                name: name,
                                status: false
                            })
                        }
                        fetch(" http://localhost:1337/update", object)
                        .then(function (response) {
                            return response.json();
                        })
                        .then(callback=>{
                            dispatch(updateTodo({
                                ...todo,
                                name: name
                            }))
    
                        })
                        if(editable) {
                         setName(todo.name);   
                        }
                        setEditable(!editable);
                    }}
                >{editable?"Update":"Edit"}</button>
                <button className="btn btn-danger m-2"
                    onClick={() =>{
                        let object = {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify({
                                id: todo.id,
                            })    
                        }
                        fetch("http://localhost:1337/delete", object)
                        .then(function (response) {
                            return response.json();
                        })
                        .then(callback=>{
                            dispatch(deleteTodo(todo.id))   
                        })
                    }
                                               
                        }
                >Delete</button>
            </div>
        </div>
    )
}

export default TodoItem
