import React, { useState } from 'react';
import { addTodo } from '../redux_toolkit/TodoSlice';
import {v1 as uuid} from 'uuid';
import { useDispatch } from 'react-redux';

function TodoInput() {
    let [name, setName] = useState();
    let dispatch = useDispatch();
    return (
        <div>
            <div className="row m-2">
                <input
                
                value={name}
                onChange={(e)=>setName(e.target.value)}
                type="text" className="col form-control"/>
                <button
                
                onClick={()=>
                    { 
                        let object = {
                            method: 'POST',
                            headers: { 'Content-Type': 'application/json' },
                            body: JSON.stringify({
                                name: name
                            })
                        }
                        fetch("http://localhost:1337/create", object)
                            .then(function (response) {
                                return response.json();
                            })
                            .then(callback=>{
                                dispatch(  addTodo({
                                            id: uuid(),
                                            name: name
                                        }));
                            })
                            setName('');
                    }
                      
                    }
                className="btn btn-primary mx-2">Add</button>
            </div>
        </div>
    )
}

export default TodoInput
