import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { deleteTodo, updateTodo } from "../redux_toolkit/TodoSlice";
import request from "../service/request";

function TodoItem({ todo, sTT }) {
  const [editable, setEditable] = useState(false);
  const [name, setName] = useState(todo.name);
  let dispatch = useDispatch();

  return (
    <div>
      <div className="row mx-4 align-items-center">
        <div>{sTT}</div>
        <div className="col">
          {editable ? (
            <input
              type="text"
              className="form-control"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            />
          ) : (
            <h4>{name}</h4>
          )}
        </div>
        <button
          className="btn btn-primary m-2"
          onClick={async () => {
            let job = {
              id: todo.id,
              name: name,
              status: false,
            };
            let rs = await request.request("/update", job, "POST");
            dispatch(
              updateTodo({
                id: todo.id,
                name: name,
                status: false,
              })
            );
            setEditable(!editable);
          }}
        >
          {editable ? "Update" : "Edit"}
        </button>
        <button
          className="btn btn-danger m-2"
          onClick={async() => {
            let job1 = {
              id: todo.id,
            },
           result = await request.request("/delete", job1, "POST");
            dispatch(deleteTodo({id: todo.id}));
            
          }}
        >
          Delete
        </button>
      </div>
    </div>
  );
}

export default TodoItem;
