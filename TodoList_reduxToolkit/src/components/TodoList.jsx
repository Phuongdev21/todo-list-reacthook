import React, { useState, useEffect } from "react";
import TodoItem from "./TodoItem";
import { useSelector } from "react-redux";
import { HOST } from "./confic";
import { useDispatch } from 'react-redux';
import { listTodo} from '../redux_toolkit/TodoSlice';
import request from "../service/request";

function TodoList() {
  let todos = useSelector((state) => state.todos.todos);
  const [data, setdatas] = useState([]);
  const dispatch = useDispatch();
  useEffect(() => {
    fectchData();
  }, []);

  const fectchData = async () => {
    let rs = await request.request("/list", {}, "GET");
    dispatch(listTodo(rs.todos));
  };

  
  return (
    <div className="my-4">
      {todos.map((todo, index) => {
        return <TodoItem key={index} todo={todo} sTT={index} />;
      })}
    </div>
  
  );
}

export default TodoList;
