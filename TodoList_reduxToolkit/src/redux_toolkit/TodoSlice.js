import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import request from "../service/request";

export const fetchPost = createAsyncThunk("getData", async (api) => {
  try {
    let rs = await request.request(api, {}, "POST");
    return rs;
  } catch (error) {
    console.log(error);
  }
});

export const deleteTodo1 = createAsyncThunk('delete', async (api, { rejectWithValue }) => {
  try {
    const todo =await  request.request(api.deleteTodo, "POST", { id: api.id });
      return todo;
  } catch (error) {
      rejectWithValue(error.response.data);
  }
})
export const updateTodo1 = createAsyncThunk('update', async (api, { rejectWithValue }) => {
  try {
      const todo = await request.request(api.updateTodo, "POST", api);
      return todo;
  } catch (error) {
      rejectWithValue(error.response.data);
  }
})

export const success = createAsyncThunk('success', async (api, { rejectWithValue }) => {
  try {
      const todo =await request.request(api, {}, "POST");
      return todo;
  } catch (error) {
      rejectWithValue(error.response.data);
  }
})


export const TodoSlice = createSlice({
  name: "todos",
  initialState: { todos: [] },
  reducers: {
    addTodo: (state, action) => {
      state.todos.push(action.payload);
      return state;
    },
    listTodo: (state, action) => {
      state.todos = action.payload;
      return state;
    },
    updateTodo: (state, action) => {
      // state.splice(action.payload.index, 1, action.payload.newValue);

      const index = state.todos.findIndex((z) => {
        return z.id === action.payload.id;
      });
      state[index] = action.payload;
      return state;
    },
    deleteTodo: (state, action) => {
      state.todos.splice(action.payload, 1);
    },
  },
  extraReducers: {
    [deleteTodo1.fulfilled]: (state, action) => {
      state.todos.splice(action.payload, 1);
    },
    [updateTodo1.fulfilled]: (state, action) => {
      const index = state.todos.findIndex((z) => {
        return z.id === action.payload.id;
      });
      state[index] = action.payload;
      return state;
    },
  },

    [fetchPost.pending]: (state, action) => {
      state.loading = "pending";
    },
    [fetchPost.fulfilled]: (state, action) => {
      state.loading = "success";
    },
    [fetchPost.rejected]: (state, action) => {
      state.loading = "error";
    },
});

// Action creators are generated for each case reducer function
export const { addTodo, listTodo, updateTodo, deleteTodo } = TodoSlice.actions;

export default TodoSlice.reducer;
